import re

def cArgToPascal(cArg):
    strippedArg = cArg.strip()
    argName = strippedArg.split()[-1]
    argType = ' '.join(strippedArg.split()[0:-1])
    pascalType = ''
    try:
        pascalType = {
            'const uint8_t': 'ctypes.cuint8',
            'struct TLVContainer*': 'TLVContainerPtr',
            'const char *': 'PChar',
            'uint16_t': 'ctypes.cuint16',
            'const int32_t': 'ctypes.cint32',
            'const uint8_t *': 'ctypes.pcuint8',
            'const struct FVLN': 'FVLN'
        }[argType]
    except KeyError:
        print('Unhandled c-type ' + argType)
        exit(1)
    return argName + ':' + pascalType

def cArgsToPascal(cArgs) :
    argList = []
    for cArg in cArgs:
        argList.append(cArgToPascal(cArg))
    return ','.join(argList)
    

with open("tlv_p_proto.h", encoding='utf-8', mode='r') as proto:
    counter = 0
    namesFileName = "pascalNames"
    functionsFileName = "pascalFunctions"
    pascalNamesFile = open(namesFileName,"w")
    pascalNamesFile.truncate()
    pascalFunctionsFile = open(functionsFileName,"w")
    pascalFunctionsFile.truncate()

    for line in proto:
        if line.startswith("TLV_FORMATTER_API") == False:
            continue
        counter = counter + 1
        searchRes = re.search(r'.* (.*)\((.*)\).*', line)
        funcName = searchRes.group(1).strip()
        funcArgs = searchRes.group(2).split(',')
        toNamesStr = "'" + funcName + "'"
        toFunctionsStr = '_' + funcName + ' : function('
        toFunctionsStr = toFunctionsStr + cArgsToPascal(funcArgs) + '):ctypes.cuint16; cdecl;'
        pascalNamesFile.write(toNamesStr + '\n')
        pascalFunctionsFile.write(toFunctionsStr + '\n')
    pascalNamesFile.write(str(counter))
    pascalFunctionsFile.write(str(counter))
    print("Success! Created '{}' and '{}' files".format(namesFileName, functionsFileName))