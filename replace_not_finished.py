        if urlparse.urlparse(self.portal_url).scheme != "https":
            return Client(wsdl_url, transport=transport)

        response = urllib2.urlopen(wsdl_url)
        wsdl_data = response.read()
        search_res = re.search(r'.*soap:address[ \t]*location[ \t]*=[ \t]*"(.*)"', wsdl_data)
        location_url = search_res.group(1).strip()
        if urlparse.urlparse(location_url).scheme == "https":
            return Client(wsdl_url, transport=transport)

        location_url = change_url_scheme(location_url, "https")

        wsdl_data = re.sub(r'.*soap:address[ \t]*location[ \t]*=[ \t]*"(.*)"',
                           '<soap:address location="%s"' % location_url, wsdl_data)
        wsdl_file = tempfile.NamedTemporaryFile()  # removed as soon as wsdl_file is garbage collected
        wsdl_file.write(wsdl_data)
        wsdl_file.flush()
        return Client("file://%s" % wsdl_file.name, transport=transport)