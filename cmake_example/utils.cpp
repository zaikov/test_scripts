#include "utils.h"

#include <iostream>
#include <fstream>
#include <io.h>
#include <ctime>

std::string commandToString(proto::CommandTypeEnum value)
{
  switch(value)
  {
  case proto::COMMAND_TICKET            : return "TICKET";
  case proto::COMMAND_CLOSE_SHIFT       : return "CLOSE_SHIFT";
  case proto::COMMAND_REPORT            : return "REPORT";
  case proto::COMMAND_MONEY_PLACEMENT   : return "MONEY_PLACEMENT";
  case proto::COMMAND_CANCEL_TICKET     : return "CANCEL_TICKET";
  case proto::COMMAND_SYSTEM            : return "SYSTEM";
  case proto::COMMAND_NOMENCLATURE      : return "NOMENCLATURE";
  case proto::COMMAND_INFO              : return "INFO";
  case proto::COMMAND_AUTH              : return "AUTH";
  default:
    break;
  }

  return "Unknown command";
}

bool copyFile(const std::string& _from, const std::string& _to)
{
  std::ifstream inputFileStream(_from, std::ifstream::in | std::ifstream::binary);
  std::ofstream outputFileStream(_to,	std::ofstream::out | std::ofstream::binary | std::ofstream::trunc);
  if (!inputFileStream.is_open() || !outputFileStream.is_open())
    return false;

  outputFileStream << inputFileStream.rdbuf();
  inputFileStream.close();
  outputFileStream.close();
  return true;
}

std::string timestampStr()
{
  time_t rawtime;
  struct tm timeinfo;
  char buffer[80];

  time(&rawtime);
  localtime_s(&timeinfo, &rawtime);

  strftime(buffer, 80, "%d_%m_%Y_%I_%M_%S", &timeinfo);
  std::string str(buffer);
  return str;
}
