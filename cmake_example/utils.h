#pragma once

#include <string>
#include <private\libvpm.pb.h>

using namespace kkm;

std::string commandToString(proto::CommandTypeEnum value);
bool copyFile(const std::string& _from, const std::string& _to);
std::string timestampStr();