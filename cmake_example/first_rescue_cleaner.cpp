#include <direct.h>
#include <iostream>
#include <fstream>
#include <io.h>
#include <tchar.h>

#include <private\libvpm.pb.h>

#include "utils.h"

using namespace kkm;

int _tmain(int argc, _TCHAR* argv[])
{				
  proto::LibVPMStateV2 v2state;

  // Find first *.state file
  _finddata_t findData;
  if (_findfirst("libvpm*.state", &findData) != -1) {
    std::cout << "Found state file: " << findData.name << "\n";
  } else {
    std::cout << "Failed to find any state file in current directory\n";
    std::cout << "Press any key...\n";
    getchar();
    return 0;
  }
  std::string filename = findData.name;
  std::cout << "Opening state file: " << filename << "\n";

  // Open and parse found *.state file
  std::ifstream inputFileStream(filename, std::ifstream::in | std::ifstream::binary);
  if (!inputFileStream.is_open()) {
    std::cout << "Failed to open state file: " << filename << "\n";
    std::cout << "Press any key...\n";
    getchar();
    return 0;
  }
  if (!v2state.ParsePartialFromIstream(&inputFileStream)) {
    std::cout << "Failed to parse state file: " << filename << "\n";
    return 0;
  }
  if (v2state.rescue_queue_size() == 0) {
    std::cout << "Rescue queue is empty\n";
    std::cout << "Press any key...\n";
    getchar();
    return 0;
  }
  inputFileStream.close();

  std::cout << "Rescue queue size is " << v2state.rescue_queue_size() << "\n";
  std::cout << "First rescue request is " << commandToString(v2state.rescue_queue().Get(0).request().command()) << "\n";

  // Confirmation
  std::cout << "Are you sure you want to remove first rescue queue request (yes/no)?\n";
  std::string answer;
  std::cin >> answer;
  std::transform(answer.begin(), answer.end(), answer.begin(), ::tolower);
  bool yes = (answer == "yes" || answer == "y");
  if (!yes)
    return 0;
  fflush(stdin);

  std::string currentTimestampStr = timestampStr();

  // Making backup
  std::string backupName = std::string(filename + ".backup_" + currentTimestampStr);
  if (!copyFile(filename, backupName))
    std::cout << "Failed to create backup file: " << backupName << "\n";

  // Opening output file for writing
  std::ofstream outputFileStream;
  outputFileStream.open(filename, std::ofstream::out | std::ofstream::binary | std::ofstream::trunc);
  if (!outputFileStream.is_open()) {
    std::cout << "Cannot write to file: " << filename << "\n";
    std::cout << "Press any key...\n";
    getchar();
    return 0;
  }

  // Save first rescue request which will be deleted
  proto::LibVPMStateV2_RescueRequest deletedRequest = v2state.mutable_rescue_queue()->Get(0);
  std::ofstream deletedRequestFile("deleted_request_" + currentTimestampStr,
                                   std::ofstream::out | std::ofstream::binary | std::ofstream::trunc);;
  deletedRequestFile << deletedRequest.request().Utf8DebugString();
  deletedRequestFile.close();

  // Delete first rescue request and write to file
  v2state.mutable_rescue_queue()->DeleteSubrange(0, 1);
  outputFileStream.clear();
  v2state.SerializePartialToOstream(&outputFileStream);
  outputFileStream.close();
  std::cout << "Successfully removed first ticket from rescue queue!\n";

  std::cout << "Press any key...\n";
  getchar();
  return 0;
}

