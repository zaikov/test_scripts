rmdir /S /Q build_dir
mkdir build_dir
pushd build_dir
cmake -G "Visual Studio 10" -DLIBVPM_PROTO_DIR="C:/work/kassa/libvpm_qtbuild/generated/proto" -DLIBVPM_EXTERNAL_BUILD_DIR="C:/work/kassa/libvpm/build/external" ".."
popd
