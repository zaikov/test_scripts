from __future__ import unicode_literals
from threading import Thread
import Queue
import logging
from django.conf import settings


logger = logging.getLogger(__name__)


def _build_worker_pool(queue, size):
    workers = []
    for _ in range(size):
        worker = _Worker(queue)
        workers.append(worker)
    return workers


class _Worker(Thread):
    def __init__(self, queue):
        Thread.__init__(self)
        self._queue = queue
        self.start()

    def run(self):
        while True:
            func, args = self._queue.get()
            func(*args)


TaskQueue = Queue.Queue()
Workers = _build_worker_pool(TaskQueue, int(settings.THREAD_COUNT))
MaxThreadCount = int(settings.MAX_THREAD_COUNT)


def add_task(function_name, function_args):
    TaskQueue.put((function_name, function_args))
    if TaskQueue.qsize() > len(Workers) and len(Workers) < MaxThreadCount:
        logger.warn("Adding new worker. Number of workers now is %s" % len(Workers))
        worker = _Worker(TaskQueue)
        Workers.append(worker)