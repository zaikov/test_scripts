@echo off

set AGENT_HOST="some_host"
set AGENT_PORT="12345"
set INI_PATH="%PROGRAMDATA%\IPHost Network Monitor Agent"

set INI_FILE=%INI_PATH%\nma.ini


copy %INI_FILE% %INI_PATH%\nma.ini.orig

net stop "IPHost Network Monitor Agent"

sed -i '/\[ActiveAgentSettings\]/,+4d' %INI_FILE%
printf "\n[ActiveAgentSettings]" >> %INI_FILE%
printf "\nTransport=ssl" >> %INI_FILE%
printf "\nPort=%AGENT_PORT%" >> %INI_FILE%
printf "\nHost=%AGENT_HOST%" >> %INI_FILE%


net start "IPHost Network Monitor Agent"