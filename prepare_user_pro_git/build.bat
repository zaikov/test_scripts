@echo off
  
rem Main script for building only release files and installers with minimum rebuild:
rem First update sources from svn (src/ must be checked out from svn),
rem then build changed files and don't build utilities.
  
rem Update scripts
rem svn up scripts
  
rem Run build script with necessary configuration
pushd scripts

call "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin\vcvars32.bat"
set PATH=D:\iphost_dev\DiskQ-msvc2013\qt\bin;C:\cygwin\bin;%PATH%
set NMAKE=D:\iphost_dev\bin\jom.exe
set NMSROOT=C:\work\netmon\iphost-desktop.git  
set IPHM_MSVC=msvc2013
set IPHM_INCLUDE="&quot;C:\WinDDK\7600.16385.1\inc\atl71&quot;"
set IPHM_LIBPATH="&quot;C:\WinDDK\7600.16385.1\lib\ATL\i386&quot;"
  
buildall.bat "-config=-dev" %*
popd