@echo off

set COPY_DIR=C:\work\netmon\iphost-desktop.git\src
set TOP_DIR=C:\work\netmon\iphost-desktop.git

copy db_upgrader.pro.user "%COPY_DIR%\db\db_upgrader"
copy mod_nms.pro.user "%COPY_DIR%\Apache\module"
copy nms_core.pro.user "%COPY_DIR%\plugins\core"
copy NMSAgent.pro.user "%COPY_DIR%\Agents\Agent"
copy NMSAgentGUI.pro.user "%COPY_DIR%\Agents\AgentGUI"
copy NMSClient.pro.user "%COPY_DIR%\Client"
copy NMSService.pro.user "%COPY_DIR%\Service"

copy build.bat "%TOP_DIR%"
copy build-settings-dev.sh "%TOP_DIR%\scripts"
copy build-settings-dev.sh_module "%TOP_DIR%\scripts"
copy build-settings-dev.sh1 "%TOP_DIR%\scripts"

