#! /bin/sh

setoption all no
setoption none no

setoption config "dev"
setoption qtdir "Q:/qt"
setoption update no
setoption checkout no
setoption clean no
setoption renew no
setoption cert no
setoption crypt no
setoption installer no
setoption freeinstaller no
setoption agentinstaller no
setoption debug yes
setoption release no
setoption free no
setoption rebuild yes
setoption compile yes

setoption fbserver copy
setoption qt copy
setoption apache yes
setoption libs yes
setoption nmsfdb yes
setoption utils yes
setoption mp3player yes
setoption agent yes
setoption agentgui yes
setoption ieprotocolproxydll no
setoption ieprotocolproxydll_agent no
setoption modnms yes
setoption plugins yes
setoption upgrader yes
setoption client yes
setoption service yes
setoption help no

#setoption delay 5

#setoption pfxinfo "C:/iphost_dev/cert/itelsib-sert.pfx"
#setoption firebird "D:/iphost_dev/DiskQ-msvc2012/firebird/"
#setoption fbport 3055
#setoption helpcomp "C:/Program Files (x86)/HTML Help Workshop/hhc.exe"
#setoption execryptor "C:/Program Files/EXECryptor/EXECrypt.exe"
#setoption installshield "C:/Program Files/InstallShield/2010/System/IsCmdBld.exe"
#setoption dxsdk "C:/Program Files (x86)/Microsoft SDKs/Windows/v7.1A"

setoption mailto "alexander.zaikov@prowidelabs.com"
setoption continue yes
setoption rev ""

