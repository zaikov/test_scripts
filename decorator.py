
def request_validator(function):
    def wrapper(*args, **kwargs):
        print "decor1"
        new_args = []
        for arg in args:
            if not type(arg) == str:
                arg = str(arg)
            new_args.append(arg)
        function(*new_args, **kwargs)
    return wrapper

@request_validator
def print_val(val, other_val):
    print(type(val))
    print(type(other_val))


print_val("123", 456)