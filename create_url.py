import urlparse

def dict_to_query(d):
  query = ''
  for key in d.keys():
    query += str(key) + '=' + str(d[key]) + "&"
  return query[:-1]

query = {"some" : "1", "other" : "2"}
print(urlparse.urlsplit('https://api.hipchat.com/v2/'))
print(urlparse.urlunsplit(("https", "api.hipchat.com", "v2", dict_to_query(query), "")))
