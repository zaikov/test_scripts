#include "MainWindow.h"

#include <QVBoxLayout>
#include <QApplication>
#include <QDesktopWidget>

#include "DB/DBWrapper.h"
#include "Logger/Logger.h"
#include "TitleWidget.h"
#include "PageStack.h"
#include "SelectPatientsPage.h"
#include "BottomPanel.h"
#include "SelectPatientsPage.h"
#include "GuiStyles.h"

#include <QMouseEvent>

static const QString geometryName = "WindowGeometry";
static const QString splitterPosName = "SplitterPos";

MainWindow::MainWindow(QWidget* _parent) :
    QWidget(_parent)
{
    QVBoxLayout* mainLay = new QVBoxLayout(this);
    setWindowFlags(Qt::Window);
    mainLay->setContentsMargins(0, 0, 0, 0);
    mainLay->setSpacing(0);
    mainLay->addWidget(m_titleWidget = new TitleWidget);
    mainLay->addWidget(m_pageStack = new PageStack);
    mainLay->addWidget(m_bottomPanel = new BottomPanel);

    connect(m_pageStack, SIGNAL(logoutRequested()), this, SIGNAL(logoutRequested()));
    connect(m_pageStack, SIGNAL(roomSelectionDone()), this, SIGNAL(roomSelectionDone()));
    connect(m_pageStack, SIGNAL(breakOutRequested(TRoomId)), this, SIGNAL(breakOutRequested(TRoomId)));
    connect(m_pageStack, SIGNAL(breakOutFinished(TRoomId)), this, SIGNAL(breakOutFinished(TRoomId)));
    connect(m_bottomPanel, SIGNAL(settingsClicked()), m_pageStack, SLOT(onSettingsClicked()));
    connect(m_bottomPanel, SIGNAL(sideBarClicked()), this, SLOT(onSideBarClicked()));

    DB::Setting geometry;
    if (DBInstance()->getSetting(geometryName, geometry))
    {
        restoreGeometry(QByteArray::fromBase64(geometry.value.toLatin1()));
    }
    else
    {
        // initial launch: give main window 95% of desktop width and 80% of height
        QDesktopWidget *desktop = QApplication::desktop();
        QRect desktopRect = desktop->availableGeometry(desktop->primaryScreen());

        move(desktopRect.x() + desktopRect.width() * .025, desktopRect.y() + desktopRect.height() * .1);
        resize(desktopRect.width() * .95, desktopRect.height() * .8);
    }

    SelectPatientsPage *selectPatientsPage = static_cast<SelectPatientsPage *>(m_pageStack->currentWidget());
    DB::Setting splitterPos;
    if (DBInstance()->getSetting(splitterPosName, splitterPos))
    {
        selectPatientsPage->restoreSplitterPosition(QByteArray::fromBase64(splitterPos.value.toLatin1()));
    }
    else
    {
        // initial table width from design spec
        selectPatientsPage->setTableWidth(GuiStyles::PatientsTableInitialWidth);
    }
    m_sideBar = new SelectPatientsPage(true, this);
    m_sideBar->setTableWidth(GuiStyles::PatientsTableInitialWidth);
    m_sideBar->transparentWidget()->installEventFilter(this);
}

void MainWindow::setCurrentPage(EMainWindowPage _page)
{
    m_pageStack->setCurrentPage(_page);
    m_titleWidget->onMainPageChanged(_page);
    m_bottomPanel->onMainPageChanged(_page);
}

void MainWindow::getVideoFrame(WId &_handle, QRect &_rect)
{
    m_pageStack->getVideoFrame(_handle, _rect);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    DB::Setting geometry;
    geometry.name = geometryName;

    // initialize "id" field if setting exists
    DBInstance()->getSetting(geometryName, geometry);

    // store updated geometry in DB
    geometry.value = QString::fromLatin1(saveGeometry().toBase64());
    if (!DBInstance()->setSetting(geometry))
    {
        Log::warning("Could not save main window geometry");
    }

    DB::Setting splitterPos;
    splitterPos.name = splitterPosName;

    // initialize "id" field if setting exists
    DBInstance()->getSetting(splitterPosName, splitterPos);

    // store updated splitter pos in DB
    m_pageStack->setCurrentPage(RoomSelection);
    SelectPatientsPage *selectPatientsPage = static_cast<SelectPatientsPage *>(m_pageStack->currentWidget());
    splitterPos.value = QString::fromLatin1(selectPatientsPage->splitterPosition().toBase64());
    if (!DBInstance()->setSetting(splitterPos))
    {
        Log::warning("Could not save splitter position");
    }

    QWidget::closeEvent(event);
}

void MainWindow::resizeEvent(QResizeEvent* _e)
{
    QWidget::resizeEvent(_e);
    if (m_sideBar->isVisible())
        m_sideBar->resize(m_pageStack->size());
}
#include <QDebug>

bool MainWindow::eventFilter(QObject* _obj, QEvent* _event)
{
    if (_obj == m_sideBar->transparentWidget() && (
                _event->type() == QEvent::MouseButtonPress ||
                _event->type() == QEvent::MouseButtonRelease ||
                _event->type() == QEvent::MouseButtonDblClick))
    {
        for (auto widget : m_pageStack->currentWidget()->findChildren<QWidget*>("RoomWidget"))
        {
            QMouseEvent* old = dynamic_cast<QMouseEvent*>(_event);
            QPoint widgetGlobalPos = widget->mapToGlobal(QPoint(0,0));
            QRect widgetGlobalRect = QRect(widgetGlobalPos.x(), widgetGlobalPos.y(),
                                           widget->width(), widget->height());
            if (widgetGlobalRect.contains(old->globalPos()))
            {
                Log::debug("!!!!!!");
                qDebug() << widgetGlobalRect << old->globalPos();
                QMouseEvent* copied = new QMouseEvent(old->type(), widget->mapFromGlobal(old->globalPos()),
                                                      old->button(), old->buttons(), old->modifiers());
                QApplication::postEvent(widget, copied);
            }
            //Log::debug(widget->
        }
    }
    return QWidget::eventFilter(_obj, _event);
}

void MainWindow::onSideBarClicked()
{
    m_sideBar->raise();
    m_sideBar->resize(m_pageStack->size());
    m_sideBar->move(m_pageStack->pos());
    m_sideBar->setVisible(!m_sideBar->isVisible());
}

