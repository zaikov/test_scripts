#ifndef ROOMWIDGET_H
#define ROOMWIDGET_H

#include <QWidget>

#include "Common/ESitterTypes.h"
#include "DB/DBWrapper.h"

namespace Ui {
    class RoomWidget;
}

class RoomWidget : public QWidget
{
    Q_OBJECT
public:
    explicit RoomWidget(const DB::Room _room, DB::Patient _patient,
                        DB::ERoomType _sitterRoom, QWidget *parent = 0);
    ~RoomWidget();
    void initialize();

public slots:
    void onPrivacyStopped();
    void onPrivacySecondsLeft(quint64 _seconds);
    void onBreakOutFinished();
    void onCallState(ECallState _callState);
    void onEndPointState(EEndpointState _endpointState);
    void onInitializeSucceeded();
    void onEndpointDisplayName(const QString& _displayName);
    void onEndpointUserName(const QString& _userName);
    void reconnect();
    void onSitterRoomReady(DB::ERoomType _roomType, const QString& _roomUserName);

signals:
    // User request BreakOut call
    void breakOutRequested(TRoomId);

    // QWidget interface
protected:
    virtual void mousePressEvent(QMouseEvent* _e);
    virtual void mouseReleaseEvent(QMouseEvent* _e);

private slots:
    void onPrivacyRequested(TRoomId _roomId, int _seconds);
    void onPrivacyIntervalChanged(TRoomId _roomId, int _seconds);
    void onStopPrivacyRequested(TRoomId _roomId);
    void onBreakOutRequested();

private:
    void updateText();
    void setRoomMode(ERoomMode _mode);
    void tryJoinToConference();

private:
    bool m_privacyOn;
    DB::Room m_room;
    DB::Patient m_patient;
    ERoomMode m_roomMode;
    DB::ERoomType m_sitterRoom;
    QString m_conferenceName;

    bool m_mousePressed;
    quint64 m_secondsLeft;

    Ui::RoomWidget* ui;

};

#endif // ROOMWIDGET_H
