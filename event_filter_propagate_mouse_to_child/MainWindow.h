#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>

#include "Common/ESitterTypes.h"

class TitleWidget;
class PageStack;
class BottomPanel;
class SelectPatientsPage;

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* _parent = nullptr);
    void setCurrentPage(EMainWindowPage _page);
    void getVideoFrame(WId &_handle, QRect &_rect);

signals:
    // Finished room selection
    void roomSelectionDone();
    // User request BreakOut call
    void breakOutRequested(TRoomId _roomId);
    // User stopped BreakOut call
    void breakOutFinished(TRoomId _roomId);
    // User wants to logoutRequested
    void logoutRequested();

protected:
    virtual void closeEvent(QCloseEvent *event);
    virtual void resizeEvent(QResizeEvent* _e);
    virtual bool eventFilter(QObject *_obj, QEvent *_event);
private slots:
    void onSideBarClicked();

private:
    TitleWidget* m_titleWidget;
    PageStack* m_pageStack;
    BottomPanel* m_bottomPanel;
    SelectPatientsPage* m_sideBar;

};

#endif // MAINWINDOW_H
