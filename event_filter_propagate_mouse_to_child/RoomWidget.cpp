#include "RoomWidget.h"

#include <QMouseEvent>

#include "ui_RoomWidget.h"
#include "RoomControlDialog.h"
#include "PrivacyManager/PrivacyManager.h"
#include "Logger/Logger.h"
#include "RemoteControl/RemoteControlManager.h"


RoomWidget::RoomWidget(const DB::Room _room, DB::Patient _patient, DB::ERoomType _sitterRoom, QWidget *parent) :
    QWidget(parent)
    , ui(new Ui::RoomWidget)
    , m_room(_room)
    , m_patient(_patient)
    , m_secondsLeft(0)
    , m_privacyOn(false)
    , m_sitterRoom(_sitterRoom)
{    
    setObjectName("RoomWidget");
    ui->setupUi(this);
    setAutoFillBackground(true);
    setStyleSheet("QWidget { background-color: grey }");
    setRoomMode(ERoomMode::NotConnected);
}

RoomWidget::~RoomWidget()
{
    delete ui;
}

void RoomWidget::initialize()
{
    RemoteControlInstance()->initialize(m_room.id, m_room.rcHost, DefaultRoomPort,
                                        m_room.rcUsername, m_room.rcPassword);
}

void RoomWidget::mousePressEvent(QMouseEvent* _e)
{
    QWidget::mousePressEvent(_e);
    if (_e->button() == Qt::LeftButton)
        m_mousePressed = true;
}

void RoomWidget::mouseReleaseEvent(QMouseEvent* _e)
{
    QWidget::mousePressEvent(_e);
    if (_e->button() != Qt::LeftButton)
        return;
    RoomControlDialog dlg(m_room, m_patient, m_secondsLeft);
    connect(&dlg, SIGNAL(breakOutRequested(TRoomId)), this, SLOT(onBreakOutRequested()));
    connect(&dlg, SIGNAL(privacyRequested(TRoomId, int)), this, SLOT(onPrivacyRequested(TRoomId,int)));
    connect(&dlg, SIGNAL(privacyIntervalChanged(TRoomId, int)), this, SLOT(onPrivacyIntervalChanged(TRoomId,int)));
    connect(&dlg, SIGNAL(stopPrivacyRequested(TRoomId)), this, SLOT(onStopPrivacyRequested(TRoomId)));
    dlg.exec();

    m_mousePressed = false;
}

void RoomWidget::onPrivacyRequested(TRoomId _roomId, int _seconds)
{
    Log::debug(tr("Starting privacy"), Log::LE_Privacy_Start, &m_room);
    m_privacyOn = true;
    PrivacyInstance()->startPrivacy(_roomId, _seconds);
}

void RoomWidget::onPrivacyIntervalChanged(TRoomId _roomId, int _seconds)
{
    Log::debug(tr("Changing privacy interval"), Log::LE_Privacy_ChangeByOperator, &m_room);
    m_privacyOn = true;
    PrivacyInstance()->startPrivacy(_roomId, _seconds);
}

void RoomWidget::onStopPrivacyRequested(TRoomId _roomId)
{
    Log::debug(tr("Stopping privacy"), Log::LE_Privacy_End, &m_room);
    PrivacyInstance()->stopPrivacy(_roomId);
}

void RoomWidget::onBreakOutRequested()
{
    setRoomMode(ERoomMode::BreakOut);
    emit breakOutRequested(m_room.id);
}

void RoomWidget::onPrivacyStopped()
{
    updateText();
}

void RoomWidget::onPrivacySecondsLeft(quint64 _seconds)
{
    m_secondsLeft = _seconds;
    updateText();
}

void RoomWidget::onBreakOutFinished()
{
    setRoomMode(ERoomMode::NotConnected);
    reconnect();
}

void RoomWidget::onCallState(ECallState _callState)
{
    // ignore during break out
    if (m_roomMode == ERoomMode::BreakOut)
        return;

    switch (_callState)
    {
    case Ringing:
        break;
    case Joining:
        break;
    case Joined:
        setRoomMode(ERoomMode::JoinedToRoom);
        break;
    case Leaving:
        break;
    case Idle:
        tryJoinToConference();
        break;
    default:
        break;
    }
}

void RoomWidget::onEndPointState(EEndpointState _endpointState)
{
    // ignore during break out
    if (m_roomMode == ERoomMode::BreakOut)
        return;

    switch (_endpointState)
    {
    case SignedOut:
        break;
    case NotConnected:
        break;
    case Connected:
        break;
    case Online:
        setRoomMode(ERoomMode::Connected);
        break;
    default:
        break;
    }
}

void RoomWidget::onInitializeSucceeded()
{
    if (m_roomMode == ERoomMode::NotConnected) {
        setRoomMode(ERoomMode::Connected);
        RemoteControlInstance()->getStatus(m_room.id);
    }
}

void RoomWidget::onEndpointDisplayName(const QString& _displayName)
{
    m_room.portalDisplaynameCached = _displayName;
    DBInstance()->setRoom(m_room);
}

void RoomWidget::onEndpointUserName(const QString& _userName)
{
    m_room.portalUsernameCached = _userName;
    DBInstance()->setRoom(m_room);
}

void RoomWidget::reconnect()
{
    if (m_roomMode == ERoomMode::NotConnected)
    {
        initialize();
    }
    else if (m_roomMode == ERoomMode::Connected)
    {
        tryJoinToConference();
    }
    else if (m_roomMode == ERoomMode::JoinedToRoom)
    {
        RemoteControlInstance()->leave(m_room.id);
    }
}

void RoomWidget::onSitterRoomReady(DB::ERoomType _roomType, const QString& _roomUserName)
{
    if (_roomType == m_sitterRoom)
    {
        m_conferenceName = _roomUserName;
        if (m_roomMode == ERoomMode::JoinedToRoom)
            RemoteControlInstance()->leave(m_room.id);
        else
            tryJoinToConference();
    }
}

void RoomWidget::updateText()
{
    ui->topLabel->setText(m_room.portalDisplaynameCached);
    if (m_privacyOn) {
	int minutes = m_secondsLeft / 60;
	if (minutes == 0)
	    minutes = 1;
        ui->centralLabel->setText("Privacy Mode Active");
#if _DEBUG
	ui->bottomLabel->setText(QString("%1 seconds").arg(m_secondsLeft));
#else
        ui->bottomLabel->setText(QString("%1 minute%2 to go").arg(minutes).arg(minutes > 1 ? "s" : ""));
#endif
    } else {
        ui->centralLabel->setText(m_patient.name);
        ui->bottomLabel->setText(m_patient.mrn);
    }
#if _DEBUG
    QString roomMode;
    switch (m_roomMode)
    {
    case ERoomMode::NotConnected:
        roomMode = "NotConnected";
        break;
    case ERoomMode::Connected:
        roomMode = "Connected";
        break;
    case ERoomMode::JoinedToRoom:
        roomMode = "JoinedToRoom";
        break;
    }
    ui->bottomLabel->setText(ui->bottomLabel->text() + " " + roomMode);
#endif
}

void RoomWidget::setRoomMode(ERoomMode _mode)
{
    m_roomMode = _mode;
    updateText();
}

void RoomWidget::tryJoinToConference()
{
    if (m_conferenceName.isEmpty())
        return;
    if (m_roomMode == ERoomMode::Connected)
        RemoteControlInstance()->join(m_room.id, m_conferenceName);
}
