import urlparse

def _change_url_scheme(url):    
    parsed = list(urlparse.urlparse(url))
    parsed[0] = "https"    
    url = urlparse.urlunparse(parsed).replace("///", "//")
    return url

url = "https://servicenow.api.vidyocloud.com/services/v1_1/VidyoPortalAdminService/"
if urlparse.urlsplit(url).scheme != "https":
    url = _change_url_scheme(url)
print(url)
