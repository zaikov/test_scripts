#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import os
import pyparsing as pp


def main():
    arg_parser = argparse.ArgumentParser('Generate setters for p-proto in C')
    arg_parser.add_argument('--out_dir', help='Output directory', type=str, default='./')
    arg_parser.add_argument('proto_file', help='container.proto location', type=str, default='./container.proto')
    args = arg_parser.parse_args()

    all_objects = []

    parse_protofile(args.proto_file, all_objects)
    gen_tlv_p_proto_h(all_objects, args.out_dir)
    gen_tlv_p_proto_c(all_objects, args.out_dir)


def to_camel_case(snake_str):
    return "".join(x[0].upper() + x[1:] for x in snake_str.split('_'))


class TLVItem:
    """ Any item in proto that have tag
    """

    def __init__(self, struct_name):
        self.type = ''
        self.tag = ''
        self.name = ''
        self.description = ''
        self.fixed_size = ''
        self.struct_name = struct_name

    # Statement parser
    modifier = pp.Or(['required', 'optional', 'repeated'])
    type_id = pp.Word(pp.alphanums)
    field_name = pp.Word(pp.alphanums + '_')
    tag = pp.Word(pp.nums)
    length = pp.Word(pp.nums)
    extended_chars = pp.srange(r"[\0x80-\0x7FF]")
    description = pp.OneOrMore(pp.Word(pp.alphas8bit + extended_chars))

    # required string inn = 1008 [(fixed_length) = 12]; // ИНН
    statement_parser = modifier + type_id + field_name + "=" + tag + pp.Optional('[(fixed_length) =' + length + ']') \
                       + ';' + '//' + description

    # Proto types to C code mapping
    known_type_setter = {
        'declaration': 'uint16_t add{struct_name}{field_name}(struct TLVContainer* container, const {c_type} value)',
        'definition': 'return addTLV{type}(container, {tag}, value);'
    }
    known_length_setter = {
        'declaration': 'uint16_t add{struct_name}{field_name}(struct TLVContainer* container, const {c_type} value)',
        'definition': 'return addTLV{type}(container, {tag}, {length}, value);'
    }
    unknown_length_setter = {
        'declaration': 'uint16_t add{struct_name}{field_name}(struct TLVContainer* container, const {c_type} value, uint16_t length)',
        'definition': 'return addTLV{type}(container, {tag}, length, value);'
    }

    fixed_string_setter = {
        'declaration': 'uint16_t add{struct_name}{field_name}(struct TLVContainer* container, const {c_type} value, uint16_t length)',
        'definition': 'return addTLV{type}(container, {tag}, length, {length}, value);'
    }

    fixed_bytes_setter = {
        'declaration': 'uint16_t add{struct_name}{field_name}(struct TLVContainer* container, const {c_type} value, uint16_t length)',
        'definition': 'return addTLV{type}(container, {tag}, length, {length}, value);'
    }

    object_setter = {
        'declaration': 'uint16_t init{struct_name}{field_name}(struct TLVContainer* container, struct TLVContainer* result)',
        'definition': 'return initTLV{type}(container, {tag}, result);'
    }

    known_type_doxy_template = \
        '/**' + '\n' + \
        ' * @brief Добавление поля {tag} - {description}' + '\n' + \
        ' * @param container Контейнер TLV' + '\n' + \
        ' * @param value Значение поля' + '\n' + \
        ' * @return 0 при успешном выполнении, или код ошибки' + '\n' + \
        ' */'

    known_length_doxy_template = \
        '/**' + '\n' + \
        ' * @brief Добавление поля {tag} - {description}' + '\n' + \
        ' * @param container Контейнер TLV' + '\n' + \
        ' * @param value Значение поля' + '\n' + \
        ' * @note поле фиксированного размера {fixed_length} байт' + '\n' + \
        ' * @return 0 при успешном выполнении, или код ошибки' + '\n' + \
        ' */'

    unknown_length_doxy_template = \
        '/**' + '\n' + \
        ' * @brief Добавление поля {tag} - {description}' + '\n' + \
        ' * @param container Контейнер TLV' + '\n' + \
        ' * @param value Значение поля' + '\n' + \
        ' * @param length размер поля в байтах' + '\n' + \
        ' * @return 0 при успешном выполнении, или код ошибки' + '\n' + \
        ' */'

    object_doxy_template = \
        '/**' + '\n' + \
        ' * @brief Инициализация структуры {tag} - {description}' + '\n' + \
        ' * @param container Контейнер TLV' + '\n' + \
        ' * @param result Структура для заполнения' + '\n' + \
        ' * @return 0 при успешном выполнении, или код ошибки' + '\n' + \
        ' */'

    # effectively all present in proto v.82 types are integers [byte, fixed32, uint64, double],
    # strings [fixed_string, string] and structures [TLVContainer]

    types_config = {
        'object': {
            'c_type': 'struct TLVContainer',
            'setter': object_setter,
            'doxy': object_doxy_template
        },
        'string': {
            'c_type': 'char *',
            'setter': unknown_length_setter,
            'doxy': unknown_length_doxy_template
        },
        'fixed_string': {
            'c_type': 'char *',
            'setter': fixed_string_setter,
            'doxy': unknown_length_doxy_template
        },
        'double': {
            'c_type': 'struct FVLN',
            'setter': known_type_setter,
            'doxy': known_type_doxy_template
        },
        'byte': {
            'c_type': 'uint8_t',
            'setter': known_type_setter,
            'doxy': known_type_doxy_template
        },
        'fixed32': {
            'c_type': 'int32_t',
            'setter': known_type_setter,
            'doxy': known_type_doxy_template
        },
        'uint64': {
            'c_type': 'int32_t',
            'setter': known_type_setter,
            'doxy': known_type_doxy_template
        },
        'bytes': {
            'c_type': 'uint8_t *',
            'setter': unknown_length_setter,
            'doxy': unknown_length_doxy_template
        },
        'fixed_bytes': {
            'c_type': 'uint8_t *',
            'setter': fixed_bytes_setter,
            'doxy': unknown_length_doxy_template
        }
    }

    def parse_field(self, field_string):
        kw = self.statement_parser.parseString(field_string)

        self.type = kw[1]
        self.name = kw[2]
        # skip '='
        self.tag = kw[4]

        if kw[5] == ';':
            self.description = ' '.join(kw[7:])
        else:
            self.fixed_size = kw[6]
            self.description = ' '.join(kw[10:])

        if self.fixed_size == '1' and self.type in ['int32', 'uint32', 'uint64']:
            self.type = 'byte'
        elif self.fixed_size != '' and self.type == 'string':
            self.type = 'fixed_string'
        elif self.fixed_size != '' and self.type == 'bytes':
            self.type = 'fixed_bytes'

        if self.type not in self.types_config:
            self.type = 'object'

    def get_setter_declaration(self):
        return self.types_config[self.type]['setter']['declaration'].format(
                struct_name=self.struct_name,
                field_name=to_camel_case(self.name),
                c_type=self.types_config[self.type]['c_type']
        )

    def get_setter_definition(self):
        return self.types_config[self.type]['setter']['definition'].format(
                type=to_camel_case(self.type),
                tag=self.tag,
                length=self.fixed_size
        )

    def get_doxy(self):
        return self.types_config[self.type]['doxy'].format(
                description=self.description,
                tag=self.tag,
                fixed_length=self.fixed_size
        )


def parse_protofile(proto_file, all_objects):
    with open(proto_file, encoding='utf-8', mode='r') as proto:
        current_message = ''
        for line in proto:
            instruction = line.strip()

            # skip proto options, etc
            if current_message == '':
                if not instruction.startswith('message'):
                    continue

            if instruction.startswith('message'):
                pm = instruction.split()
                current_message = pm[1]

            if instruction.startswith('optional') \
                    or instruction.startswith('required') \
                    or instruction.startswith('repeated'):
                stmt = TLVItem(current_message)
                stmt.parse_field(instruction)
                all_objects.append(stmt)


def gen_tlv_p_proto_h(all_objects, out_dir):
    lines = [
        '#ifndef __TLV_P_PROTO_H',
        '#define __TLV_P_PROTO_H',
        '/**',
        ' * @file tlv_p_proto.h',
        ' * @brief Функции добавления полей P-протокола',
        ' */',
        '',
        '#include "tlv_types.h"',
        '',
        '#ifdef __cplusplus',
        'extern "C" {',
        '#endif',
        ''
    ]

    for o in all_objects:
        lines.append('')
        lines.append(o.get_doxy())
        lines.append('TLV_FORMATTER_API ' + o.get_setter_declaration() + ';')

    lines.append('')
    lines.append('#ifdef __cplusplus')
    lines.append('} // extern "C"')
    lines.append('#endif')
    lines.append('')
    lines.append('#endif // __TLV_P_PROTO_H')
    lines.append('')

    with open(os.path.join(out_dir, 'tlv_p_proto.h'), encoding='utf-8', mode='w') as hdr:
        for line in lines:
            print(line, file=hdr)


def gen_tlv_p_proto_c(all_objects, out_dir):
    lines = [
        '/**',
        ' * @file tlv_p_proto.c',
        ' * @brief Функции добавления полей P-протокола',
        ' */',
        '',
        '#include "tlv_p_proto.h"',
        '#include "tlv_types_internal.h"'
        ''

    ]

    for o in all_objects:
        lines.append('')
        lines.append(o.get_setter_declaration())
        lines.append('{')
        lines.append('    ' + o.get_setter_definition())
        lines.append('}')

    lines.append('')

    with open(os.path.join(out_dir, 'tlv_p_proto.c'), encoding='utf-8', mode='w') as src:
        for line in lines:
            print(line, file=src)


if __name__ == "__main__":
    main()
