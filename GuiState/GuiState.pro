#-------------------------------------------------
#
# Project created by QtCreator 2016-04-08T16:27:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GuiState
TEMPLATE = app


SOURCES += main.cpp\
        CMainWidget.cpp \
    CMainStateMachine.cpp \
    CLoginDialog.cpp

HEADERS  += CMainWidget.h \
    CMainStateMachine.h \
    CLoginDialog.h
