#include "CLoginDialog.h"

CLoginDialog::CLoginDialog(QWidget *parent) : QDialog(parent)
{
    QVBoxLayout* mainLay = new QVBoxLayout(this);
    mainLay->addWidget(m_login = new QPushButton("Login"));
    resize(200, 400);

    connect(m_login, SIGNAL(clicked(bool)), this, SIGNAL(loginSucceeded()));
}
