#ifndef CMAINSTATEMACHINE_H
#define CMAINSTATEMACHINE_H

#include <QStateMachine>

class CLoginDialog;
class CMainWidget;

class CMainStateMachine : public QStateMachine
{
    Q_OBJECT
public:
    explicit CMainStateMachine(CLoginDialog* _loginDialog, CMainWidget* _mainWidget,
			       QObject* parent = nullptr);

    ~CMainStateMachine();

private slots:
    void onStateStart();
    void onStateMainWidget();
    void onStateLogin();
private:
    QState m_startState;
    QState m_loginState;
    QState m_mainWidgetState;

    CLoginDialog* m_loginDialog;
    CMainWidget* m_mainWidget;
};

#endif // CMAINSTATEMACHINE_H
