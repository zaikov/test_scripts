#ifndef CLOGINDIALOG_H
#define CLOGINDIALOG_H

#include <QDialog>

#include <QVBoxLayout>
#include <QPushButton>

class CLoginDialog : public QDialog
{
    Q_OBJECT
public:
    explicit CLoginDialog(QWidget *parent = 0);

signals:
    void loginSucceeded();

private:
    QPushButton* m_login;

};

#endif // CLOGINDIALOG_H
