#include "CMainWidget.h"
#include "CMainStateMachine.h"
#include "CLoginDialog.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    CMainStateMachine stateMachine(new CLoginDialog, new CMainWidget);
    stateMachine.start();

    return a.exec();
}
