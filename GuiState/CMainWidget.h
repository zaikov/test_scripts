#ifndef CMAINWIDGET_H
#define CMAINWIDGET_H

#include <QWidget>

class CMainWidget : public QWidget
{
    Q_OBJECT

public:
    CMainWidget(QWidget *parent = 0);
    ~CMainWidget();
};

#endif // CMAINWIDGET_H
