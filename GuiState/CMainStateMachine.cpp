#include "CMainStateMachine.h"

#include <QWidget>
#include <QDebug>

#include "CLoginDialog.h"
#include "CMainWidget.h"

CMainStateMachine::CMainStateMachine(CLoginDialog* _loginDialog, CMainWidget* _mainWidget, QObject *parent) :
    QStateMachine(parent)
    , m_loginDialog(_loginDialog)
    , m_mainWidget(_mainWidget)
{
    addState(&m_startState);
    addState(&m_loginState);
    addState(&m_mainWidgetState);

    setInitialState(&m_startState);

    m_startState.addTransition(&m_startState, SIGNAL(entered()), &m_loginState);
    m_loginState.addTransition(m_loginDialog, SIGNAL(loginSucceeded()), &m_mainWidgetState);

    connect(&m_startState, SIGNAL(entered()), this, SLOT(onStateStart()));
    connect(&m_loginState, SIGNAL(entered()), this, SLOT(onStateLogin()));
    connect(&m_mainWidgetState, SIGNAL(entered()), this, SLOT(onStateMainWidget()));
}

CMainStateMachine::~CMainStateMachine()
{
    qDebug() << "dtor";

    delete m_loginDialog;
    delete m_mainWidget;
}

void CMainStateMachine::onStateStart()
{
    qDebug() << "CMainStateMachine::stateOnStart()";
    m_mainWidget->hide();
}

void CMainStateMachine::onStateMainWidget()
{
    qDebug() << "CMainStateMachine::onStateMainWidget()";
    m_loginDialog->hide();
    m_mainWidget->show();
}

void CMainStateMachine::onStateLogin()
{
    qDebug() << "CMainStateMachine::onStateLogin()";
    m_loginDialog->show();
}
