import threading
import sys

import msvcrt
import time

import random
import string

import os

os.system('color')


def randomString():
    """Generate a random string of fixed length """
    stringLength = random.randint(10, 100)
    letters = string.ascii_lowercase + string.ascii_uppercase + (" " * random.randint(10, 200))
    return ''.join(random.choice(letters) for i in range(stringLength))

run = True

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
 
def doubler():

    global run
    """
    A function that can be used by a thread
    """
    while run:        
        c = msvcrt.getch()    
        print(str(c))
        run = False
    print("DONE")

 
if __name__ == '__main__':
    my_thread = threading.Thread(target=doubler, args=())
    my_thread.start()

while run:
    time.sleep(0.0001)
    print(bcolors.OKGREEN + randomString() + bcolors.ENDC, end='')

c = msvcrt.getch()    