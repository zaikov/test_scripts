from __future__ import print_function
import sys


def exit_with_error(error_text):
    print(error_text, file=sys.stderr)
    sys.exit(1)

if 'success' == str(sys.argv[1]).strip().lower():
    print("success")
    sys.exit(0)
else:
    print("failed")
    exit_with_error("failed")
