import SimpleHTTPServer
import SocketServer
import BaseHTTPServer
import urlparse
import time

PORT = 8000

#Handler = SimpleHTTPServer.SimpleHTTPRequestHandler

class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    def __init__(self, *args, **kwargs):
        self.request_handlers = {
            '/': self.get_main_handler,
            '/get_normal': self.get_common,
            '/get_file': self.get_common,
            '/get_hidden': self.get_common,
            '/get_hidden_spec': self.get_hidden_spec,
            '/after_get_hidden_spec': self.after_get_hidden_spec,           
            '/get_headers': self.get_headers,
            '/get_dir': self.get_dir
        }
        BaseHTTPServer.BaseHTTPRequestHandler.__init__(self, *args, **kwargs)

    def do_GET(s):
        print("\n### headers start:\n" + str(s.headers) + "### headers end.\n")
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        #s.send_header("Content-type", "application/octet-stream")
        s.end_headers()
        query = urlparse.urlparse(s.path)
        if query.path in s.request_handlers:
            s.request_handlers[query.path]()
        else:
            s.get_main_handler()
    def get_dir(s):
        file = open("rufus-2.9.exe", "r")
        s.wfile.write(file.read())
    def get_main_handler(s):
        file = open("index.html", "r")
        s.wfile.write(file.read())

    def get_headers(s):
        s.wfile.write("Headers:\n");
        for header in str(s.headers).split("\n"):
            s.wfile.write("<br>" + header)

    def get_common(s):        
        s.wfile.write("Success\n")

    def get_hidden_spec(s):        
        file = open("get_hidden_spec.html", "r") 
        s.wfile.write(file.read())

    def after_get_hidden_spec(s):        
        s.wfile.write("Success\n")
        
    def do_POST(s):
        print("\n### headers start:\n" + str(s.headers) + "### headers end.\n")
        length = int(s.headers.getheader('content-length'))
        field_data = s.rfile.read(length)
        print(field_data)
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
        s.wfile.write("Success. Post data:<br>")
        s.wfile.write(str(field_data))
        return

      
httpd = SocketServer.TCPServer(("", PORT), MyHandler)

print("serving at port", PORT)
httpd.serve_forever()

