from os import listdir
from os.path import isfile, join
import re

def get_xml_value(data, xml_element):
    return re.search(r'.*<%s>(.*)</%s>.*' % (xml_element, xml_element), file_data).group(1).strip()


category_notes = {}
template_notes = {}

for f in listdir("."):   
    if isfile(f) and f.endswith(".xml"):
        file_data = open(f, mode='r').read()
        category_notes[get_xml_value(file_data, "Category")] = get_xml_value(file_data, "CategoryNotes")
        template_notes[get_xml_value(file_data, "DisplayName")] = get_xml_value(file_data, "Notes")

for k, v in category_notes.iteritems():
    print('templateCategoryNameToNote.insert("%s", "%s");' % (k, v))

for k, v in template_notes.iteritems():
    print('templateNameToNote.insert("%s", "%s");' % (k, v))