#!/usr/bin/env python

import asyncio
import websockets


async def hello():
    #async with websockets.connect("ws://localhost:8765") as websocket:
    async with websockets.connect("ws://127.0.0.1:8080/store-websocket") as websocket:
        await websocket.send("resp")
        res = await websocket.recv()
        print("response: " + res)

asyncio.run(hello())
