import json
import yaml
from kafka import KafkaConsumer

with open('settings.yaml') as f:
    settings = yaml.load(f, Loader=yaml.FullLoader)
print(settings)

# consumer = KafkaConsumer(settings['kafka.consumer.topic'],
#                          group_id=settings['kafka.consumer.group'],
#                          bootstrap_servers=settings['kafka.host'],
#                          value_deserializer=lambda m: json.loads(m.decode('utf-8')))

consumer = KafkaConsumer(settings['kafka.consumer.topic'],
                         group_id=settings['kafka.consumer.group'],
                         bootstrap_servers=settings['kafka.host'])

for message in consumer:
    print("%s:%d:%d: key=%s value=%s headers=%s" % (message.topic, message.partition,
                                         message.offset, message.key,
                                         message.value, message.headers))
