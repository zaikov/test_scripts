import json
import yaml
from kafka import KafkaProducer

with open('settings.yaml') as f:
    settings = yaml.load(f, Loader=yaml.FullLoader)
print(settings)

producer = KafkaProducer(bootstrap_servers=settings['kafka.host'],
                         value_serializer=lambda v: json.dumps(v).encode('utf-8'))

# payload = {
#     'request_id': 'fcb700e5-432f-44fc-8eb3-7ee700570024',
#     'type': 'enrich',
#     'data': {
#         'fields': ['first_name'],
#         'users': ['f4d71713-4fc8-4253-aab4-15907a55ba15']
#     }
# }

payload = {
    'request_id': 'fcb700e5-432f-44fc-8eb3-7ee700570024',
    'type': 'bulkRate',
    'data': {
        'bulks': ['e4b37605-4f5b-4dd7-ba67-ed80bf281af4', 'f0e313e7-dcad-4593-bc14-db05d38f9829'],
        'rateType': 'orderRate'
    }
}
for i in range(0, 100):
    producer.send(settings['kafka.producer.topic'], payload)
    producer.flush()

print('done')
