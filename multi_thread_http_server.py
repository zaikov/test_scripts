from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
from threading import Thread, Lock
import threading
import time

mutex = Lock()
activeConnections = 0

def updateActiveConnections(increase):
    with mutex:
        global activeConnections
        activeConnections += increase
        print 'Active connections', activeConnections

class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        try:
            updateActiveConnections(1)
            time.sleep(7)
            self.send_response(200)
            self.end_headers()
        finally:
            updateActiveConnections(-1)
        return

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

if __name__ == '__main__':
    server = ThreadedHTTPServer(('localhost', 5454), Handler)
    print 'Starting server, use <Ctrl-C> to stop'
    server.serve_forever()