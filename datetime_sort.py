from datetime import datetime

from collections import defaultdict
import operator

settings = defaultdict(list)

settings['setting_gas'].append((datetime.strptime('Jun 1 2005  1:33PM', '%b %d %Y %I:%M%p'), 23))
settings['setting_gas'].append((datetime.strptime('Jun 1 2007  1:33PM', '%b %d %Y %I:%M%p'), 11))
settings['setting_gas'].append((datetime.strptime('Jun 1 2006  1:33PM', '%b %d %Y %I:%M%p'), 33))

print(settings['setting_gas'])
settings['setting_gas'] = sorted(settings['setting_gas'], key=operator.itemgetter(0))
print(settings['setting_gas'][-1][1])

