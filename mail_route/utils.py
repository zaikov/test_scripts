from __future__ import print_function
import sys
import time

def exit_with_error(error_text):
    print(error_text, file=sys.stderr)
    sys.exit(1)

def exit_successfully():
    print('Mail successfully delivered', file=sys.stdout)
    sys.exit(0)

def check_timeout():
    if (time.time() - check_timeout.start_time > 60):
        exit_with_error('Error: timeout exceeded')
check_timeout.start_time = time.time()
