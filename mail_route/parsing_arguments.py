import argparse

def parse_arguments():
    parser = argparse.ArgumentParser(description='This is a mail route script')
    parser.add_argument('smtp_host', help = 'SMTP server host address')
    parser.add_argument('smtp_port', help = 'SMTP server port')
    parser.add_argument('smtp_from_mail', help = 'Test mail "from" address')
    parser.add_argument('smtp_to_mail', help = 'Test mail "to" address')
    parser.add_argument('-smtp_login', default='', help = 'Login if SMTP server requires authentication')
    parser.add_argument('-smtp_password', default='', help = 'Password if SMTP server requires authentication')
    parser.add_argument('-smtp_connection_type', default='none', help = 'Secure connection type for SMTP server. Avalable values "none", "ssl", "tls", "none" by default')

    parser.add_argument('imap_host', help = 'IMAP server host address')
    parser.add_argument('imap_port', help = 'IMAP server port')
    parser.add_argument('-imap_login', help = 'IMAP server login')
    parser.add_argument('-imap_password', help = 'IMAP server password')
    parser.add_argument('-imap_mail_folder', default='INBOX', help = 'Mail folder where the test email will be stored. INBOX folder by default')
    parser.add_argument('-imap_connection_type', default='tls', help = 'Secure connection type for IMAP server. Avalable values "none", "ssl", "tls", "tls" by default')
    
    return parser.parse_args()
