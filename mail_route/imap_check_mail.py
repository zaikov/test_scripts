# -*- coding: utf-8 -*-
from imap_starttls import IMAP4_STARTTLS
import imaplib
import time


class ImapMailChecker:
    def __init__(self, host, port, login, password, secure_connection_type='tls'):
        conn_type = secure_connection_type.strip().lower()
        if conn_type == 'none':
            self.server = IMAP4(host=host, port=port)
        elif conn_type == 'ssl':
            self.server = IMAP4_SSL(host=host, port=port)
        else:
            self.server = IMAP4_STARTTLS(host=host, port=port)
        if login:
            self.server.login(login, password)

    def mail_exists(self, test_mail_subject, msg_id, mail_folder = 'INBOX'):
        self.server.select(mail_folder)
        typ, data = self.server.search(None, '(SUBJECT "%s")' % test_mail_subject)
        for mail_number in data[0].split():
            typ, fetch_data = self.server.fetch(mail_number, '(BODY[HEADER.FIELDS (MESSAGE-ID)])')
            print(msg_id)
            print(fetch_data[0][1])
            if msg_id in str(fetch_data[0][1]):
                self.server.store(mail_number, '+FLAGS', '\\Deleted')
                self.server.expunge()
                return True
        return False
            
    def __del__(self):
        if hasattr(self, 'server'):
            self.server.close()
            self.server.logout()

#imap_server = ImapMailChecker(imap_server_host, imap_server_port, imap_login, imap_password)
#print(imap_server.mail_exists(test_mail_subject, msg_id, 'Iphost service'))
