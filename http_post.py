import requests

webhook_url = "https://hooks.slack.com/services/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"

s = """
    "attachments": [
        {
            "fallback": "Required plain-text summary of the attachment.",
            "color": "#36a64f",
            "text": "Optional text that appears within the attachment"
        }
    ]
    """

r = requests.post(webhook_url, data='{"username": "IPHost", "text": "IPHost - OK", "icon_url": "https://cdn.iphostmonitor.com/images/icons/40x40/iphost.png", %s}' %s)
#r = requests.post(webhook_url, data={'payload':'{"text": "from python"}'})
print(r.status_code, r.reason)
print(r.text)

