# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from Crypto.Cipher import AES
from Crypto import Random
import base64
import hashlib


secret_key = ""


def set_secret_key(new_secret_key):
    global secret_key
    secret_key = hashlib.md5(new_secret_key.encode("utf-8")).hexdigest()[:16]

set_secret_key("6e89d8c0fd340d78f50698f3b6562b6b")


def to_bytes(s):
    return ":".join("{:02x}".format(ord(c)) for c in s)


def encrypt(msg):
    msg = msg.encode("utf-8")
    init_vector = Random.new().read(AES.block_size)
    cipher = AES.new(secret_key, AES.MODE_CFB, init_vector)
    msg = init_vector + cipher.encrypt(msg)
    return msg.encode("hex")


def decrypt(msg):
    init_vector = msg[:AES.block_size]
    cipher = AES.new(secret_key, AES.MODE_CFB, init_vector)    
    return cipher.decrypt(msg.decode("hex"))[AES.block_size:]


def whatisthis(s):
    if isinstance(s, str):
        print "ordinary string"
    elif isinstance(s, unicode):
        print "unicode string"
    else:
        print "not a string"
    return None

print(decrypt('5d689a45e15bf88bc8ba06aeedcbc9e29bac82c07f5701'))

