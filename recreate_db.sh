#!/bin/sh

bd_user_name="vidyo_user"
bd_user_password="vidyo_slack"
bd_name="vidyodb"
manage_py="/home/zaikov/vidyo-integration/manage.py"
superuser="admin"
superuser_password="adminadmin"


sudo -u postgres psql postgres -c "DROP DATABASE $bd_name;"
sudo -u postgres psql postgres -c "DROP USER $bd_user_name;"
sudo -u postgres psql postgres -c "CREATE USER vidyo_user;"
sudo -u postgres psql postgres -c "CREATE DATABASE vidyodb OWNER vidyo_user;"
sudo -u postgres psql postgres -c "alter user $bd_user_name with password '$bd_user_password';"
sudo python $manage_py migrate
echo "from django.contrib.auth.models import User; User.objects.create_superuser('$superuser', '', '$superuser_password')" | python $manage_py shell


