
def unmangled_portal_room_name(name):
    parts = name.split("#")
    if len(parts) != 3:
        return name
    else:
        return '#'.join(parts[:-1])


name1 = 'Hipchat_Some_Team#Some_room'

print(unmangled_portal_room_name(name1))