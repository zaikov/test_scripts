import socket
import time


import threading

class print_out(threading.Thread):

    def __init__ (self):
        threading.Thread.__init__(self)

    def run(self):
        print "someFunc was called"
        TCP_IP = '127.0.0.1'
        TCP_PORT = 1554
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((TCP_IP, TCP_PORT))
        time.sleep(50)
        print "\n"

threads = []
for num in range(0, 100):
    thread = print_out()
    thread.start()
    threads.append(thread)

for thread in threads:
    thread.join()
 
#print "!!!!!!!"